from django.urls import path,include

urlpatterns = [
    path('api/', include('laundry.apis.api_urls'))
]
