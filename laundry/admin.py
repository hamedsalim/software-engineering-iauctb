from django.contrib import admin
from laundry.models import Services,LaundryProfile,Comment,Star
# Register your models here.
@admin.register(Services)
class AdminServices(admin.ModelAdmin):
    pass

@admin.register(LaundryProfile)
class AdminLaundryProfile(admin.ModelAdmin):
    pass

@admin.register(Comment)
class AdminComment(admin.ModelAdmin):
    pass

@admin.register(Star)
class AdminStar(admin.ModelAdmin):
    pass