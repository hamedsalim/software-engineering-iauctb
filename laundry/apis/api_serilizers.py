from rest_framework import serializers
from customer.models import Services,Comment,Star
class ServicesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Services
        fields = ("title","price","discount_price","category","slug","description","image")

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields =  '__all__'

class StarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Star
        fields =  '__all__'