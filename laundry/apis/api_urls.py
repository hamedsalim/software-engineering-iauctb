from django.urls import path
from .api_views import DetailServices,ListServices,ListComment,DetailComment,ListStar,DetailStar
urlpatterns = [
    path('services/', ListServices.as_view()),
    path('services/<str:slug>/', DetailServices.as_view()),
    path('comments/', ListComment.as_view()),
    path('comments/<int:pk>/', DetailComment.as_view()),
    path('start/', ListStar.as_view()),
    path('start/<int:pk>/', DetailStar.as_view()),
]

