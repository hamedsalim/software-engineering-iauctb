from django.db import models
from base.models import Timestamp 
from django.shortcuts import reverse
from django.core.validators import MaxValueValidator, MinValueValidator
CATEGORY_CHOICES = (
    ('L', 'Laundry'),
    ('I', 'Ironing'),
    ('T', 'Tailoring'),
)
class Services(Timestamp):
    title = models.CharField(max_length=100)
    price = models.FloatField()
    discount_price = models.FloatField(blank=True, null=True)
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=2)
    slug = models.SlugField()
    description = models.TextField()
    image = models.ImageField(max_length=128,upload_to='var/laundry/image', height_field='height_field', width_field='width_field',blank=True, null=True)
    height_field = models.PositiveIntegerField(default='480',blank=True, null=True)
    width_field  = models.PositiveIntegerField(default='720',blank=True, null=True)


    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("laundry:detail_services", kwargs={
            'slug': self.slug
        })

    def get_add_to_cart_url(self):
        return reverse("core:add-to-cart", kwargs={
            'slug': self.slug
        })

    def get_remove_from_cart_url(self):
        return reverse("core:remove-from-cart", kwargs={
            'slug': self.slug
        })



class Comment(Timestamp):
    service = models.ForeignKey(Services,on_delete=models.CASCADE)
    name = models.CharField(max_length=80,default="No name")
    email = models.EmailField()
    body = models.TextField()
    class Meta:
        ordering = ['create']
    def __str__(self):
        return 'Comment {} by {}'.format(self.body, self.name)

class Star(Timestamp):
    service = models.ForeignKey(Services,on_delete=models.CASCADE,related_name='comments')
    name = models.CharField(max_length=80,default="No name")
    email = models.EmailField()
    star = models.PositiveIntegerField(default=0,validators = [MaxValueValidator(5),MinValueValidator(0)])
    class Meta:
        ordering = ['create']
