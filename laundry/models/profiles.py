from base.models import Profile
from .services import Services
from django.db import models
from django.core.validators import RegexValidator
class LaundryProfile(Profile):
    bio = models.TextField(max_length=500, blank=True)
    services = models.ManyToManyField(Services)
    AddressArea_services = models.ManyToManyField('base.AddressArea',related_name='+')
    activate = models.BooleanField(default=False)
    shabaNumberRegex = RegexValidator(regex = r"\d{16}$")
    shaba_number = models.CharField(validators = [shabaNumberRegex], max_length = 16, unique = True)