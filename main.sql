/*
 Navicat SQLite Data Transfer

 Source Server         : uni
 Source Server Type    : SQLite
 Source Server Version : 3030001
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3030001
 File Encoding         : 65001

 Date: 29/12/2021 12:29:50
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS "auth_group";
CREATE TABLE "auth_group" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "name" varchar(150) NOT NULL,
  UNIQUE ("name" ASC)
);

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS "auth_group_permissions";
CREATE TABLE "auth_group_permissions" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "group_id" integer NOT NULL,
  "permission_id" integer NOT NULL,
  FOREIGN KEY ("group_id") REFERENCES "auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("permission_id") REFERENCES "auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS "auth_permission";
CREATE TABLE "auth_permission" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "content_type_id" integer NOT NULL,
  "codename" varchar(100) NOT NULL,
  "name" varchar(255) NOT NULL,
  FOREIGN KEY ("content_type_id") REFERENCES "django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO "auth_permission" VALUES (1, 1, 'add_logentry', 'Can add log entry');
INSERT INTO "auth_permission" VALUES (2, 1, 'change_logentry', 'Can change log entry');
INSERT INTO "auth_permission" VALUES (3, 1, 'delete_logentry', 'Can delete log entry');
INSERT INTO "auth_permission" VALUES (4, 1, 'view_logentry', 'Can view log entry');
INSERT INTO "auth_permission" VALUES (5, 2, 'add_permission', 'Can add permission');
INSERT INTO "auth_permission" VALUES (6, 2, 'change_permission', 'Can change permission');
INSERT INTO "auth_permission" VALUES (7, 2, 'delete_permission', 'Can delete permission');
INSERT INTO "auth_permission" VALUES (8, 2, 'view_permission', 'Can view permission');
INSERT INTO "auth_permission" VALUES (9, 3, 'add_group', 'Can add group');
INSERT INTO "auth_permission" VALUES (10, 3, 'change_group', 'Can change group');
INSERT INTO "auth_permission" VALUES (11, 3, 'delete_group', 'Can delete group');
INSERT INTO "auth_permission" VALUES (12, 3, 'view_group', 'Can view group');
INSERT INTO "auth_permission" VALUES (13, 4, 'add_user', 'Can add user');
INSERT INTO "auth_permission" VALUES (14, 4, 'change_user', 'Can change user');
INSERT INTO "auth_permission" VALUES (15, 4, 'delete_user', 'Can delete user');
INSERT INTO "auth_permission" VALUES (16, 4, 'view_user', 'Can view user');
INSERT INTO "auth_permission" VALUES (17, 5, 'add_contenttype', 'Can add content type');
INSERT INTO "auth_permission" VALUES (18, 5, 'change_contenttype', 'Can change content type');
INSERT INTO "auth_permission" VALUES (19, 5, 'delete_contenttype', 'Can delete content type');
INSERT INTO "auth_permission" VALUES (20, 5, 'view_contenttype', 'Can view content type');
INSERT INTO "auth_permission" VALUES (21, 6, 'add_session', 'Can add session');
INSERT INTO "auth_permission" VALUES (22, 6, 'change_session', 'Can change session');
INSERT INTO "auth_permission" VALUES (23, 6, 'delete_session', 'Can delete session');
INSERT INTO "auth_permission" VALUES (24, 6, 'view_session', 'Can view session');
INSERT INTO "auth_permission" VALUES (25, 7, 'add_addressarea', 'Can add address area');
INSERT INTO "auth_permission" VALUES (26, 7, 'change_addressarea', 'Can change address area');
INSERT INTO "auth_permission" VALUES (27, 7, 'delete_addressarea', 'Can delete address area');
INSERT INTO "auth_permission" VALUES (28, 7, 'view_addressarea', 'Can view address area');
INSERT INTO "auth_permission" VALUES (29, 8, 'add_coupon', 'Can add coupon');
INSERT INTO "auth_permission" VALUES (30, 8, 'change_coupon', 'Can change coupon');
INSERT INTO "auth_permission" VALUES (31, 8, 'delete_coupon', 'Can delete coupon');
INSERT INTO "auth_permission" VALUES (32, 8, 'view_coupon', 'Can view coupon');
INSERT INTO "auth_permission" VALUES (33, 9, 'add_ticket', 'Can add ticket');
INSERT INTO "auth_permission" VALUES (34, 9, 'change_ticket', 'Can change ticket');
INSERT INTO "auth_permission" VALUES (35, 9, 'delete_ticket', 'Can delete ticket');
INSERT INTO "auth_permission" VALUES (36, 9, 'view_ticket', 'Can view ticket');
INSERT INTO "auth_permission" VALUES (37, 10, 'add_reply', 'Can add reply');
INSERT INTO "auth_permission" VALUES (38, 10, 'change_reply', 'Can change reply');
INSERT INTO "auth_permission" VALUES (39, 10, 'delete_reply', 'Can delete reply');
INSERT INTO "auth_permission" VALUES (40, 10, 'view_reply', 'Can view reply');
INSERT INTO "auth_permission" VALUES (41, 11, 'add_custommerprofile', 'Can add custommer profile');
INSERT INTO "auth_permission" VALUES (42, 11, 'change_custommerprofile', 'Can change custommer profile');
INSERT INTO "auth_permission" VALUES (43, 11, 'delete_custommerprofile', 'Can delete custommer profile');
INSERT INTO "auth_permission" VALUES (44, 11, 'view_custommerprofile', 'Can view custommer profile');
INSERT INTO "auth_permission" VALUES (45, 12, 'add_order', 'Can add order');
INSERT INTO "auth_permission" VALUES (46, 12, 'change_order', 'Can change order');
INSERT INTO "auth_permission" VALUES (47, 12, 'delete_order', 'Can delete order');
INSERT INTO "auth_permission" VALUES (48, 12, 'view_order', 'Can view order');
INSERT INTO "auth_permission" VALUES (49, 13, 'add_orderitem', 'Can add order item');
INSERT INTO "auth_permission" VALUES (50, 13, 'change_orderitem', 'Can change order item');
INSERT INTO "auth_permission" VALUES (51, 13, 'delete_orderitem', 'Can delete order item');
INSERT INTO "auth_permission" VALUES (52, 13, 'view_orderitem', 'Can view order item');
INSERT INTO "auth_permission" VALUES (53, 14, 'add_refund', 'Can add refund');
INSERT INTO "auth_permission" VALUES (54, 14, 'change_refund', 'Can change refund');
INSERT INTO "auth_permission" VALUES (55, 14, 'delete_refund', 'Can delete refund');
INSERT INTO "auth_permission" VALUES (56, 14, 'view_refund', 'Can view refund');
INSERT INTO "auth_permission" VALUES (57, 15, 'add_services', 'Can add services');
INSERT INTO "auth_permission" VALUES (58, 15, 'change_services', 'Can change services');
INSERT INTO "auth_permission" VALUES (59, 15, 'delete_services', 'Can delete services');
INSERT INTO "auth_permission" VALUES (60, 15, 'view_services', 'Can view services');
INSERT INTO "auth_permission" VALUES (61, 16, 'add_star', 'Can add star');
INSERT INTO "auth_permission" VALUES (62, 16, 'change_star', 'Can change star');
INSERT INTO "auth_permission" VALUES (63, 16, 'delete_star', 'Can delete star');
INSERT INTO "auth_permission" VALUES (64, 16, 'view_star', 'Can view star');
INSERT INTO "auth_permission" VALUES (65, 17, 'add_laundryprofile', 'Can add laundry profile');
INSERT INTO "auth_permission" VALUES (66, 17, 'change_laundryprofile', 'Can change laundry profile');
INSERT INTO "auth_permission" VALUES (67, 17, 'delete_laundryprofile', 'Can delete laundry profile');
INSERT INTO "auth_permission" VALUES (68, 17, 'view_laundryprofile', 'Can view laundry profile');
INSERT INTO "auth_permission" VALUES (69, 18, 'add_comment', 'Can add comment');
INSERT INTO "auth_permission" VALUES (70, 18, 'change_comment', 'Can change comment');
INSERT INTO "auth_permission" VALUES (71, 18, 'delete_comment', 'Can delete comment');
INSERT INTO "auth_permission" VALUES (72, 18, 'view_comment', 'Can view comment');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS "auth_user";
CREATE TABLE "auth_user" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "password" varchar(128) NOT NULL,
  "last_login" datetime,
  "is_superuser" bool NOT NULL,
  "username" varchar(150) NOT NULL,
  "last_name" varchar(150) NOT NULL,
  "email" varchar(254) NOT NULL,
  "is_staff" bool NOT NULL,
  "is_active" bool NOT NULL,
  "date_joined" datetime NOT NULL,
  "first_name" varchar(150) NOT NULL,
  UNIQUE ("username" ASC)
);

-- ----------------------------
-- Records of auth_user
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS "auth_user_groups";
CREATE TABLE "auth_user_groups" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "user_id" integer NOT NULL,
  "group_id" integer NOT NULL,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("group_id") REFERENCES "auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS "auth_user_user_permissions";
CREATE TABLE "auth_user_user_permissions" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "user_id" integer NOT NULL,
  "permission_id" integer NOT NULL,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("permission_id") REFERENCES "auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for base_addressarea
-- ----------------------------
DROP TABLE IF EXISTS "base_addressarea";
CREATE TABLE "base_addressarea" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "addres" text NOT NULL,
  "areacode" integer unsigned NOT NULL,
  "city" varchar(128) NOT NULL,
   ("areacode" >= 0)
);

-- ----------------------------
-- Records of base_addressarea
-- ----------------------------

-- ----------------------------
-- Table structure for base_coupon
-- ----------------------------
DROP TABLE IF EXISTS "base_coupon";
CREATE TABLE "base_coupon" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "publish_status" varchar(1) NOT NULL,
  "code" varchar(15) NOT NULL,
  "amount" real NOT NULL
);

-- ----------------------------
-- Records of base_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for base_reply
-- ----------------------------
DROP TABLE IF EXISTS "base_reply";
CREATE TABLE "base_reply" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "message" text NOT NULL,
  "ticket_id" varchar(128) NOT NULL,
  "user_id" integer NOT NULL,
  FOREIGN KEY ("ticket_id") REFERENCES "base_ticket" ("sku") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of base_reply
-- ----------------------------

-- ----------------------------
-- Table structure for base_ticket
-- ----------------------------
DROP TABLE IF EXISTS "base_ticket";
CREATE TABLE "base_ticket" (
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "sku" varchar(128) NOT NULL,
  "title" varchar(50) NOT NULL,
  "description" varchar(100) NOT NULL,
  "date_created" date NOT NULL,
  PRIMARY KEY ("sku")
);

-- ----------------------------
-- Records of base_ticket
-- ----------------------------

-- ----------------------------
-- Table structure for customer_custommerprofile
-- ----------------------------
DROP TABLE IF EXISTS "customer_custommerprofile";
CREATE TABLE "customer_custommerprofile" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "first_name" varchar(128) NOT NULL,
  "last_name" varchar(128) NOT NULL,
  "phoneNumber" varchar(11) NOT NULL,
  "birth_date" date,
  "address" text NOT NULL,
  "shaba_number" varchar(16) NOT NULL,
  "AddressArea_services_id" bigint NOT NULL,
  "user_id" integer NOT NULL,
  "vip" bool NOT NULL,
  FOREIGN KEY ("AddressArea_services_id") REFERENCES "base_addressarea" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  UNIQUE ("phoneNumber" ASC),
  UNIQUE ("shaba_number" ASC)
);

-- ----------------------------
-- Records of customer_custommerprofile
-- ----------------------------

-- ----------------------------
-- Table structure for customer_custommerprofile_address_areacode
-- ----------------------------
DROP TABLE IF EXISTS "customer_custommerprofile_address_areacode";
CREATE TABLE "customer_custommerprofile_address_areacode" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "custommerprofile_id" bigint NOT NULL,
  "addressarea_id" bigint NOT NULL,
  FOREIGN KEY ("custommerprofile_id") REFERENCES "customer_custommerprofile" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("addressarea_id") REFERENCES "base_addressarea" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of customer_custommerprofile_address_areacode
-- ----------------------------

-- ----------------------------
-- Table structure for customer_order
-- ----------------------------
DROP TABLE IF EXISTS "customer_order";
CREATE TABLE "customer_order" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "ref_code" varchar(20),
  "start_date" datetime NOT NULL,
  "ordered_date" datetime NOT NULL,
  "ordered" bool NOT NULL,
  "billing_address" text NOT NULL,
  "being_delivered" bool NOT NULL,
  "received" bool NOT NULL,
  "refund_requested" bool NOT NULL,
  "refund_granted" bool NOT NULL,
  "coupon_id" bigint,
  "shipping_address_id" bigint,
  "user_id" integer NOT NULL,
  FOREIGN KEY ("coupon_id") REFERENCES "base_coupon" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("shipping_address_id") REFERENCES "customer_custommerprofile" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of customer_order
-- ----------------------------

-- ----------------------------
-- Table structure for customer_order_items
-- ----------------------------
DROP TABLE IF EXISTS "customer_order_items";
CREATE TABLE "customer_order_items" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "order_id" bigint NOT NULL,
  "orderitem_id" bigint NOT NULL,
  FOREIGN KEY ("order_id") REFERENCES "customer_order" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("orderitem_id") REFERENCES "customer_orderitem" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of customer_order_items
-- ----------------------------

-- ----------------------------
-- Table structure for customer_orderitem
-- ----------------------------
DROP TABLE IF EXISTS "customer_orderitem";
CREATE TABLE "customer_orderitem" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "ordered" bool NOT NULL,
  "quantity" integer NOT NULL,
  "service_id" bigint NOT NULL,
  "user_id" integer NOT NULL,
  FOREIGN KEY ("service_id") REFERENCES "laundry_services" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of customer_orderitem
-- ----------------------------

-- ----------------------------
-- Table structure for customer_refund
-- ----------------------------
DROP TABLE IF EXISTS "customer_refund";
CREATE TABLE "customer_refund" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "reason" text NOT NULL,
  "accepted" bool NOT NULL,
  "price" real NOT NULL,
  "order_id" bigint NOT NULL,
  FOREIGN KEY ("order_id") REFERENCES "customer_order" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of customer_refund
-- ----------------------------

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS "django_admin_log";
CREATE TABLE "django_admin_log" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "action_time" datetime NOT NULL,
  "object_id" text,
  "object_repr" varchar(200) NOT NULL,
  "change_message" text NOT NULL,
  "content_type_id" integer,
  "user_id" integer NOT NULL,
  "action_flag" smallint unsigned NOT NULL,
  FOREIGN KEY ("content_type_id") REFERENCES "django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
   ("action_flag" >= 0)
);

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS "django_content_type";
CREATE TABLE "django_content_type" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "app_label" varchar(100) NOT NULL,
  "model" varchar(100) NOT NULL
);

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO "django_content_type" VALUES (1, 'admin', 'logentry');
INSERT INTO "django_content_type" VALUES (2, 'auth', 'permission');
INSERT INTO "django_content_type" VALUES (3, 'auth', 'group');
INSERT INTO "django_content_type" VALUES (4, 'auth', 'user');
INSERT INTO "django_content_type" VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO "django_content_type" VALUES (6, 'sessions', 'session');
INSERT INTO "django_content_type" VALUES (7, 'base', 'addressarea');
INSERT INTO "django_content_type" VALUES (8, 'base', 'coupon');
INSERT INTO "django_content_type" VALUES (9, 'base', 'ticket');
INSERT INTO "django_content_type" VALUES (10, 'base', 'reply');
INSERT INTO "django_content_type" VALUES (11, 'customer', 'custommerprofile');
INSERT INTO "django_content_type" VALUES (12, 'customer', 'order');
INSERT INTO "django_content_type" VALUES (13, 'customer', 'orderitem');
INSERT INTO "django_content_type" VALUES (14, 'customer', 'refund');
INSERT INTO "django_content_type" VALUES (15, 'laundry', 'services');
INSERT INTO "django_content_type" VALUES (16, 'laundry', 'star');
INSERT INTO "django_content_type" VALUES (17, 'laundry', 'laundryprofile');
INSERT INTO "django_content_type" VALUES (18, 'laundry', 'comment');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS "django_migrations";
CREATE TABLE "django_migrations" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "app" varchar(255) NOT NULL,
  "name" varchar(255) NOT NULL,
  "applied" datetime NOT NULL
);

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO "django_migrations" VALUES (1, 'contenttypes', '0001_initial', '2021-12-28 15:06:13.588371');
INSERT INTO "django_migrations" VALUES (2, 'auth', '0001_initial', '2021-12-28 15:06:13.624370');
INSERT INTO "django_migrations" VALUES (3, 'admin', '0001_initial', '2021-12-28 15:06:13.661689');
INSERT INTO "django_migrations" VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2021-12-28 15:06:13.676699');
INSERT INTO "django_migrations" VALUES (5, 'admin', '0003_logentry_add_action_flag_choices', '2021-12-28 15:06:13.708614');
INSERT INTO "django_migrations" VALUES (6, 'contenttypes', '0002_remove_content_type_name', '2021-12-28 15:06:13.732648');
INSERT INTO "django_migrations" VALUES (7, 'auth', '0002_alter_permission_name_max_length', '2021-12-28 15:06:13.767217');
INSERT INTO "django_migrations" VALUES (8, 'auth', '0003_alter_user_email_max_length', '2021-12-28 15:06:13.793717');
INSERT INTO "django_migrations" VALUES (9, 'auth', '0004_alter_user_username_opts', '2021-12-28 15:06:13.802692');
INSERT INTO "django_migrations" VALUES (10, 'auth', '0005_alter_user_last_login_null', '2021-12-28 15:06:13.834469');
INSERT INTO "django_migrations" VALUES (11, 'auth', '0006_require_contenttypes_0002', '2021-12-28 15:06:13.840439');
INSERT INTO "django_migrations" VALUES (12, 'auth', '0007_alter_validators_add_error_messages', '2021-12-28 15:06:13.857920');
INSERT INTO "django_migrations" VALUES (13, 'auth', '0008_alter_user_username_max_length', '2021-12-28 15:06:13.884105');
INSERT INTO "django_migrations" VALUES (14, 'auth', '0009_alter_user_last_name_max_length', '2021-12-28 15:06:13.900105');
INSERT INTO "django_migrations" VALUES (15, 'auth', '0010_alter_group_name_max_length', '2021-12-28 15:06:13.926290');
INSERT INTO "django_migrations" VALUES (16, 'auth', '0011_update_proxy_permissions', '2021-12-28 15:06:13.943290');
INSERT INTO "django_migrations" VALUES (17, 'auth', '0012_alter_user_first_name_max_length', '2021-12-28 15:06:13.965032');
INSERT INTO "django_migrations" VALUES (18, 'base', '0001_initial', '2021-12-28 15:06:13.998198');
INSERT INTO "django_migrations" VALUES (19, 'laundry', '0001_initial', '2021-12-28 15:06:14.055411');
INSERT INTO "django_migrations" VALUES (20, 'customer', '0001_initial', '2021-12-28 15:06:14.090627');
INSERT INTO "django_migrations" VALUES (21, 'customer', '0002_initial', '2021-12-28 15:06:14.350657');
INSERT INTO "django_migrations" VALUES (22, 'sessions', '0001_initial', '2021-12-28 15:06:14.379624');
INSERT INTO "django_migrations" VALUES (23, 'base', '0002_alter_ticket_options', '2021-12-29 08:59:11.188848');
INSERT INTO "django_migrations" VALUES (24, 'customer', '0003_custommerprofile_vip', '2021-12-29 08:59:11.224842');
INSERT INTO "django_migrations" VALUES (25, 'laundry', '0002_laundryprofile_vip', '2021-12-29 08:59:11.255842');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS "django_session";
CREATE TABLE "django_session" (
  "session_key" varchar(40) NOT NULL,
  "session_data" text NOT NULL,
  "expire_date" datetime NOT NULL,
  PRIMARY KEY ("session_key")
);

-- ----------------------------
-- Records of django_session
-- ----------------------------

-- ----------------------------
-- Table structure for laundry_comment
-- ----------------------------
DROP TABLE IF EXISTS "laundry_comment";
CREATE TABLE "laundry_comment" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "name" varchar(80) NOT NULL,
  "email" varchar(254) NOT NULL,
  "body" text NOT NULL,
  "service_id" bigint NOT NULL,
  FOREIGN KEY ("service_id") REFERENCES "laundry_services" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of laundry_comment
-- ----------------------------

-- ----------------------------
-- Table structure for laundry_laundryprofile
-- ----------------------------
DROP TABLE IF EXISTS "laundry_laundryprofile";
CREATE TABLE "laundry_laundryprofile" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "first_name" varchar(128) NOT NULL,
  "last_name" varchar(128) NOT NULL,
  "phoneNumber" varchar(11) NOT NULL,
  "birth_date" date,
  "address" text NOT NULL,
  "bio" text NOT NULL,
  "activate" bool NOT NULL,
  "shaba_number" varchar(16) NOT NULL,
  "user_id" integer NOT NULL,
  "vip" bool NOT NULL,
  FOREIGN KEY ("user_id") REFERENCES "auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  UNIQUE ("phoneNumber" ASC),
  UNIQUE ("shaba_number" ASC)
);

-- ----------------------------
-- Records of laundry_laundryprofile
-- ----------------------------

-- ----------------------------
-- Table structure for laundry_laundryprofile_AddressArea_services
-- ----------------------------
DROP TABLE IF EXISTS "laundry_laundryprofile_AddressArea_services";
CREATE TABLE "laundry_laundryprofile_AddressArea_services" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "laundryprofile_id" bigint NOT NULL,
  "addressarea_id" bigint NOT NULL,
  FOREIGN KEY ("laundryprofile_id") REFERENCES "laundry_laundryprofile" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("addressarea_id") REFERENCES "base_addressarea" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of laundry_laundryprofile_AddressArea_services
-- ----------------------------

-- ----------------------------
-- Table structure for laundry_laundryprofile_address_areacode
-- ----------------------------
DROP TABLE IF EXISTS "laundry_laundryprofile_address_areacode";
CREATE TABLE "laundry_laundryprofile_address_areacode" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "laundryprofile_id" bigint NOT NULL,
  "addressarea_id" bigint NOT NULL,
  FOREIGN KEY ("laundryprofile_id") REFERENCES "laundry_laundryprofile" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("addressarea_id") REFERENCES "base_addressarea" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of laundry_laundryprofile_address_areacode
-- ----------------------------

-- ----------------------------
-- Table structure for laundry_laundryprofile_services
-- ----------------------------
DROP TABLE IF EXISTS "laundry_laundryprofile_services";
CREATE TABLE "laundry_laundryprofile_services" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "laundryprofile_id" bigint NOT NULL,
  "services_id" bigint NOT NULL,
  FOREIGN KEY ("laundryprofile_id") REFERENCES "laundry_laundryprofile" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY ("services_id") REFERENCES "laundry_services" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED
);

-- ----------------------------
-- Records of laundry_laundryprofile_services
-- ----------------------------

-- ----------------------------
-- Table structure for laundry_services
-- ----------------------------
DROP TABLE IF EXISTS "laundry_services";
CREATE TABLE "laundry_services" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "title" varchar(100) NOT NULL,
  "price" real NOT NULL,
  "discount_price" real,
  "category" varchar(2) NOT NULL,
  "slug" varchar(50) NOT NULL,
  "description" text NOT NULL,
  "image" varchar(128),
  "height_field" integer unsigned,
  "width_field" integer unsigned,
   ("height_field" >= 0),
   ("width_field" >= 0)
);

-- ----------------------------
-- Records of laundry_services
-- ----------------------------

-- ----------------------------
-- Table structure for laundry_star
-- ----------------------------
DROP TABLE IF EXISTS "laundry_star";
CREATE TABLE "laundry_star" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "create" datetime NOT NULL,
  "updated" datetime NOT NULL,
  "name" varchar(80) NOT NULL,
  "email" varchar(254) NOT NULL,
  "star" integer unsigned NOT NULL,
  "service_id" bigint NOT NULL,
  FOREIGN KEY ("service_id") REFERENCES "laundry_services" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
   ("star" >= 0)
);

-- ----------------------------
-- Records of laundry_star
-- ----------------------------

-- ----------------------------
-- Table structure for sqlite_sequence
-- ----------------------------
DROP TABLE IF EXISTS "sqlite_sequence";
CREATE TABLE "sqlite_sequence" (
  "name",
  "seq"
);

-- ----------------------------
-- Records of sqlite_sequence
-- ----------------------------
INSERT INTO "sqlite_sequence" VALUES ('django_migrations', 25);
INSERT INTO "sqlite_sequence" VALUES ('django_admin_log', 0);
INSERT INTO "sqlite_sequence" VALUES ('django_content_type', 18);
INSERT INTO "sqlite_sequence" VALUES ('auth_permission', 72);
INSERT INTO "sqlite_sequence" VALUES ('auth_group', 0);
INSERT INTO "sqlite_sequence" VALUES ('auth_user', 0);
INSERT INTO "sqlite_sequence" VALUES ('customer_orderitem', 0);
INSERT INTO "sqlite_sequence" VALUES ('customer_order', 0);
INSERT INTO "sqlite_sequence" VALUES ('customer_custommerprofile', 0);
INSERT INTO "sqlite_sequence" VALUES ('laundry_laundryprofile', 0);

-- ----------------------------
-- Table structure for sqlite_stat1
-- ----------------------------
DROP TABLE IF EXISTS "sqlite_stat1";
CREATE TABLE "sqlite_stat1" (
  "tbl",
  "idx",
  "stat"
);

-- ----------------------------
-- Records of sqlite_stat1
-- ----------------------------
INSERT INTO "sqlite_stat1" VALUES ('django_migrations', NULL, '22');
INSERT INTO "sqlite_stat1" VALUES ('django_content_type', 'django_content_type_app_label_model_76bd3d3b_uniq', '18 3 1');
INSERT INTO "sqlite_stat1" VALUES ('auth_permission', 'auth_permission_content_type_id_2f476e4b', '72 4');
INSERT INTO "sqlite_stat1" VALUES ('auth_permission', 'auth_permission_content_type_id_codename_01ab375a_uniq', '72 4 1');

-- ----------------------------
-- Auto increment value for auth_group
-- ----------------------------

-- ----------------------------
-- Indexes structure for table auth_group_permissions
-- ----------------------------
CREATE INDEX "auth_group_permissions_group_id_b120cbf9"
ON "auth_group_permissions" (
  "group_id" ASC
);
CREATE UNIQUE INDEX "auth_group_permissions_group_id_permission_id_0cd325b0_uniq"
ON "auth_group_permissions" (
  "group_id" ASC,
  "permission_id" ASC
);
CREATE INDEX "auth_group_permissions_permission_id_84c5c92e"
ON "auth_group_permissions" (
  "permission_id" ASC
);

-- ----------------------------
-- Auto increment value for auth_permission
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 72 WHERE name = 'auth_permission';

-- ----------------------------
-- Indexes structure for table auth_permission
-- ----------------------------
CREATE INDEX "auth_permission_content_type_id_2f476e4b"
ON "auth_permission" (
  "content_type_id" ASC
);
CREATE UNIQUE INDEX "auth_permission_content_type_id_codename_01ab375a_uniq"
ON "auth_permission" (
  "content_type_id" ASC,
  "codename" ASC
);

-- ----------------------------
-- Auto increment value for auth_user
-- ----------------------------

-- ----------------------------
-- Indexes structure for table auth_user_groups
-- ----------------------------
CREATE INDEX "auth_user_groups_group_id_97559544"
ON "auth_user_groups" (
  "group_id" ASC
);
CREATE INDEX "auth_user_groups_user_id_6a12ed8b"
ON "auth_user_groups" (
  "user_id" ASC
);
CREATE UNIQUE INDEX "auth_user_groups_user_id_group_id_94350c0c_uniq"
ON "auth_user_groups" (
  "user_id" ASC,
  "group_id" ASC
);

-- ----------------------------
-- Indexes structure for table auth_user_user_permissions
-- ----------------------------
CREATE INDEX "auth_user_user_permissions_permission_id_1fbb5f2c"
ON "auth_user_user_permissions" (
  "permission_id" ASC
);
CREATE INDEX "auth_user_user_permissions_user_id_a95ead1b"
ON "auth_user_user_permissions" (
  "user_id" ASC
);
CREATE UNIQUE INDEX "auth_user_user_permissions_user_id_permission_id_14a6b632_uniq"
ON "auth_user_user_permissions" (
  "user_id" ASC,
  "permission_id" ASC
);

-- ----------------------------
-- Indexes structure for table base_reply
-- ----------------------------
CREATE INDEX "base_reply_ticket_id_6d3854db"
ON "base_reply" (
  "ticket_id" ASC
);
CREATE INDEX "base_reply_user_id_d7dade34"
ON "base_reply" (
  "user_id" ASC
);

-- ----------------------------
-- Auto increment value for customer_custommerprofile
-- ----------------------------

-- ----------------------------
-- Indexes structure for table customer_custommerprofile
-- ----------------------------
CREATE INDEX "customer_custommerprofile_AddressArea_services_id_de13b771"
ON "customer_custommerprofile" (
  "AddressArea_services_id" ASC
);
CREATE INDEX "customer_custommerprofile_user_id_4ea5cebc"
ON "customer_custommerprofile" (
  "user_id" ASC
);

-- ----------------------------
-- Indexes structure for table customer_custommerprofile_address_areacode
-- ----------------------------
CREATE INDEX "customer_custommerprofile_address_areacode_addressarea_id_b94c8821"
ON "customer_custommerprofile_address_areacode" (
  "addressarea_id" ASC
);
CREATE INDEX "customer_custommerprofile_address_areacode_custommerprofile_id_1ee86718"
ON "customer_custommerprofile_address_areacode" (
  "custommerprofile_id" ASC
);
CREATE UNIQUE INDEX "customer_custommerprofile_address_areacode_custommerprofile_id_addressarea_id_887b3b62_uniq"
ON "customer_custommerprofile_address_areacode" (
  "custommerprofile_id" ASC,
  "addressarea_id" ASC
);

-- ----------------------------
-- Auto increment value for customer_order
-- ----------------------------

-- ----------------------------
-- Indexes structure for table customer_order
-- ----------------------------
CREATE INDEX "customer_order_coupon_id_bd667c4a"
ON "customer_order" (
  "coupon_id" ASC
);
CREATE INDEX "customer_order_shipping_address_id_01bcba25"
ON "customer_order" (
  "shipping_address_id" ASC
);
CREATE INDEX "customer_order_user_id_360ac224"
ON "customer_order" (
  "user_id" ASC
);

-- ----------------------------
-- Indexes structure for table customer_order_items
-- ----------------------------
CREATE INDEX "customer_order_items_order_id_450beef1"
ON "customer_order_items" (
  "order_id" ASC
);
CREATE UNIQUE INDEX "customer_order_items_order_id_orderitem_id_10bb5212_uniq"
ON "customer_order_items" (
  "order_id" ASC,
  "orderitem_id" ASC
);
CREATE INDEX "customer_order_items_orderitem_id_7d49b38c"
ON "customer_order_items" (
  "orderitem_id" ASC
);

-- ----------------------------
-- Auto increment value for customer_orderitem
-- ----------------------------

-- ----------------------------
-- Indexes structure for table customer_orderitem
-- ----------------------------
CREATE INDEX "customer_orderitem_service_id_f2dd68e3"
ON "customer_orderitem" (
  "service_id" ASC
);
CREATE INDEX "customer_orderitem_user_id_cd34fb20"
ON "customer_orderitem" (
  "user_id" ASC
);

-- ----------------------------
-- Indexes structure for table customer_refund
-- ----------------------------
CREATE INDEX "customer_refund_order_id_6f708fd3"
ON "customer_refund" (
  "order_id" ASC
);

-- ----------------------------
-- Auto increment value for django_admin_log
-- ----------------------------

-- ----------------------------
-- Indexes structure for table django_admin_log
-- ----------------------------
CREATE INDEX "django_admin_log_content_type_id_c4bce8eb"
ON "django_admin_log" (
  "content_type_id" ASC
);
CREATE INDEX "django_admin_log_user_id_c564eba6"
ON "django_admin_log" (
  "user_id" ASC
);

-- ----------------------------
-- Auto increment value for django_content_type
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 18 WHERE name = 'django_content_type';

-- ----------------------------
-- Indexes structure for table django_content_type
-- ----------------------------
CREATE UNIQUE INDEX "django_content_type_app_label_model_76bd3d3b_uniq"
ON "django_content_type" (
  "app_label" ASC,
  "model" ASC
);

-- ----------------------------
-- Auto increment value for django_migrations
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 25 WHERE name = 'django_migrations';

-- ----------------------------
-- Indexes structure for table django_session
-- ----------------------------
CREATE INDEX "django_session_expire_date_a5c62663"
ON "django_session" (
  "expire_date" ASC
);

-- ----------------------------
-- Indexes structure for table laundry_comment
-- ----------------------------
CREATE INDEX "laundry_comment_service_id_ac251bb2"
ON "laundry_comment" (
  "service_id" ASC
);

-- ----------------------------
-- Auto increment value for laundry_laundryprofile
-- ----------------------------

-- ----------------------------
-- Indexes structure for table laundry_laundryprofile
-- ----------------------------
CREATE INDEX "laundry_laundryprofile_user_id_596a3d9d"
ON "laundry_laundryprofile" (
  "user_id" ASC
);

-- ----------------------------
-- Indexes structure for table laundry_laundryprofile_AddressArea_services
-- ----------------------------
CREATE INDEX "laundry_laundryprofile_AddressArea_services_addressarea_id_6250b7e1"
ON "laundry_laundryprofile_AddressArea_services" (
  "addressarea_id" ASC
);
CREATE INDEX "laundry_laundryprofile_AddressArea_services_laundryprofile_id_9006c363"
ON "laundry_laundryprofile_AddressArea_services" (
  "laundryprofile_id" ASC
);
CREATE UNIQUE INDEX "laundry_laundryprofile_AddressArea_services_laundryprofile_id_addressarea_id_a74900ad_uniq"
ON "laundry_laundryprofile_AddressArea_services" (
  "laundryprofile_id" ASC,
  "addressarea_id" ASC
);

-- ----------------------------
-- Indexes structure for table laundry_laundryprofile_address_areacode
-- ----------------------------
CREATE INDEX "laundry_laundryprofile_address_areacode_addressarea_id_4d7ebd03"
ON "laundry_laundryprofile_address_areacode" (
  "addressarea_id" ASC
);
CREATE INDEX "laundry_laundryprofile_address_areacode_laundryprofile_id_637e9887"
ON "laundry_laundryprofile_address_areacode" (
  "laundryprofile_id" ASC
);
CREATE UNIQUE INDEX "laundry_laundryprofile_address_areacode_laundryprofile_id_addressarea_id_873be874_uniq"
ON "laundry_laundryprofile_address_areacode" (
  "laundryprofile_id" ASC,
  "addressarea_id" ASC
);

-- ----------------------------
-- Indexes structure for table laundry_laundryprofile_services
-- ----------------------------
CREATE INDEX "laundry_laundryprofile_services_laundryprofile_id_dd39cf5b"
ON "laundry_laundryprofile_services" (
  "laundryprofile_id" ASC
);
CREATE UNIQUE INDEX "laundry_laundryprofile_services_laundryprofile_id_services_id_69754b65_uniq"
ON "laundry_laundryprofile_services" (
  "laundryprofile_id" ASC,
  "services_id" ASC
);
CREATE INDEX "laundry_laundryprofile_services_services_id_5336b366"
ON "laundry_laundryprofile_services" (
  "services_id" ASC
);

-- ----------------------------
-- Indexes structure for table laundry_services
-- ----------------------------
CREATE INDEX "laundry_services_slug_6ec802f0"
ON "laundry_services" (
  "slug" ASC
);

-- ----------------------------
-- Indexes structure for table laundry_star
-- ----------------------------
CREATE INDEX "laundry_star_service_id_596efc4a"
ON "laundry_star" (
  "service_id" ASC
);

PRAGMA foreign_keys = true;
