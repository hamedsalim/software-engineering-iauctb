from django.db import models
from base.models import Timestamp
from django.core.validators import MaxValueValidator, MinValueValidator



class AddressArea(Timestamp):
    addres =  models.TextField()
    areacode =  models.PositiveIntegerField(validators = [MaxValueValidator(22),MinValueValidator(1)])
    city = models.CharField(max_length=128)