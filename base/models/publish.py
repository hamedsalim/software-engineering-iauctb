from django.db import models
from base.models.timestamp import Timestamp
class Publish(Timestamp):
    STATUS = (
        ('D' , 'DRAFT'),
        ( 'P','PUBLISH'),
        )
    publish_status = models.CharField( max_length=1,
        choices=STATUS,
        default='P')
    class Meta:
        abstract = True