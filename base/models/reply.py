from django.db import models
from base.models.timestamp import Timestamp
from base.models.ticket import Ticket
from django.contrib.auth.models import User 

class Reply(Timestamp):
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.TextField()
    def __str__(self):
        return self.ticket.title
