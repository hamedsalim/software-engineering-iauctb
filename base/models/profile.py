from django.db import models
from base.models.timestamp import Timestamp
from base.models.addressarea import AddressArea
from django.conf import settings
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _

class Profile(Timestamp):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    phoneNumberRegex = RegexValidator(regex = r"^09\+?1?\d{9}$")
    phoneNumber = models.CharField(validators = [phoneNumberRegex], max_length = 11, unique = True)
    birth_date = models.DateField(null=True, blank=True)
    address = models.TextField()
    address_areacode = models.ManyToManyField(AddressArea,verbose_name=_('Address area codes'))
    vip = models.BooleanField(default=False)
    def __str__(self):
        return self.user.username
    class Meta:
        abstract = True