from django.db import models
from base.models.publish import Publish

class Coupon(Publish):
    code = models.CharField(max_length=15)
    amount = models.FloatField()

    def __str__(self):
        return self.code