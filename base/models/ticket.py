from django.db import models
from base.models.timestamp import Timestamp

class Ticket(Timestamp):
    sku = models.CharField(max_length=128,primary_key= True,unique=True)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    date_created = models.DateField(auto_now_add=True)
    
    def __str__(self):
        return self.title
