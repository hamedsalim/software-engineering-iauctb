from rest_framework import serializers
from base.models import AddressArea,Coupon,Ticket,Reply
from django.contrib.auth.models import User
class AddressAreaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AddressArea
        fields = ("addres","areacode","city")

class CouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        fields =  ("code","amount")


class TicketSerializers(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields =  '__all__'

class ReplySializers(serializers.ModelSerializer):
    thread = serializers.PrimaryKeyRelatedField(queryset=Ticket.objects.all(),allow_null=True)
    class Meta:
        model = Reply
        fields =  '__all__'

class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields =  '__all__'

