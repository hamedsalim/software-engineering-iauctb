from django.urls import path
from .api_views import ListAddressAreaServices,DetailAddressArea,DetailCoupont,ListCoupon,ListUser,DetailUser,ListTicket,DetailTicket,ListReply,DetailReply
urlpatterns = [
    path('addressarea/', ListAddressAreaServices.as_view()),
    path('addressarea/<int:pk>/', DetailAddressArea.as_view()),
    path('coupon/', ListCoupon.as_view()),
    path('coupon/<int:pk>/', DetailCoupont.as_view()),
    path('reply/', ListCoupon.as_view()),
    path('reply/<int:pk>/', DetailCoupont.as_view()),
    path('user/', ListCoupon.as_view()),
    path('user/<int:pk>/', DetailCoupont.as_view()),
    path('ticket/', ListCoupon.as_view()),
    path('ticket/<int:pk>/', DetailCoupont.as_view()),
]

