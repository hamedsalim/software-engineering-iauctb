from rest_framework import generics,permissions
from base.models import AddressArea,Coupon,Ticket,Reply
from .api_serilizers import CouponSerializer,AddressAreaSerializer,TicketSerializers,ReplySializers,UserSerializers
from rest_framework import exceptions
from django.contrib.auth.models import User
class ListAddressAreaServices(generics.ListCreateAPIView):
    queryset = AddressArea.objects.all()
    serializer_class = AddressAreaSerializer
class DetailAddressArea(generics.RetrieveUpdateDestroyAPIView):
    queryset = AddressArea.objects.all()
    serializer_class = AddressAreaSerializer


class ListCoupon(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAdminUser,)
    queryset = Coupon.objects.all()
    serializer_class = CouponSerializer
class DetailCoupont(generics.RetrieveUpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Coupon.objects.all()
    serializer_class = CouponSerializer

class ListReply(generics.ListCreateAPIView):
    queryset = Reply.objects.all()
    serializer_class = ReplySializers
class DetailReply(generics.RetrieveUpdateAPIView):
    queryset = Reply.objects.all()
    serializer_class = ReplySializers


class ListUser(generics.ListCreateAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializers
class DetailUser(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializers

class ListTicket(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializers
class DetailTicket(generics.RetrieveUpdateAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializers

