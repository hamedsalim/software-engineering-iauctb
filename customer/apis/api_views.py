from rest_framework import generics ,viewsets 
from customer.models import Refund,OrderItem,Order,CustommerProfile
from .api_serilizers import RefundSerializer,CustommerProfileSerializer,OrderSerializer,OrderItemSerializer

class ListRefund(generics.ListCreateAPIView):
    queryset = Refund.objects.all()
    serializer_class = RefundSerializer
class DetailRefund(generics.RetrieveUpdateAPIView):
    queryset = Refund.objects.all()
    serializer_class = RefundSerializer

class ListCustommerProfile(generics.ListCreateAPIView):
    queryset = CustommerProfile.objects.all()
    serializer_class = CustommerProfileSerializer
class DetailCustommerProfile(generics.RetrieveUpdateDestroyAPIView):
    queryset = CustommerProfile.objects.all()
    serializer_class = CustommerProfileSerializer

class ListOrder(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
class DetailOrder(generics.RetrieveUpdateDestroyAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
class ListOrderItem(generics.ListCreateAPIView):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
class DetailOrderItem(generics.RetrieveUpdateDestroyAPIView):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer