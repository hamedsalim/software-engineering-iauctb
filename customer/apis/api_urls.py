from django.urls import path
from .api_views import ListRefund,DetailRefund,ListCustommerProfile,DetailCustommerProfile,ListOrder,DetailOrder,ListOrderItem,DetailOrderItem
urlpatterns = [
    path('refund/', ListRefund.as_view()),
    path('refund/<int:pk>/', DetailRefund.as_view()),
    path('profile/', ListCustommerProfile.as_view()),
    path('profile/<int:pk>/', DetailCustommerProfile.as_view()),
    path('order/', ListOrder.as_view()),
    path('order/<int:pk>/', DetailOrder.as_view()),
    path('orderitems/', ListOrderItem.as_view()),
    path('orderitems/<int:pk>/', DetailOrderItem.as_view()),
]

