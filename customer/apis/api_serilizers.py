from rest_framework import serializers
from customer.models import Refund,OrderItem,Order,CustommerProfile
class RefundSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Refund
        fields = ("order","reason","accepted","price")

class CustommerProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustommerProfile
        fields =  ("AddressArea_services","shaba_number")

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields =  (
            "user",
            "ref_code",
            "items",
            "start_date",
            "ordered_date",
            "ordered",
            "shipping_address",
            "billing_address",
            "coupon",
            "being_delivered",
            "received",
            "refund_requested",
            "refund_granted"
        )

class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields =  (
                "user",
                "ordered",
                "service",
                "quantity"
            )