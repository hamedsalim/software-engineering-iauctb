from django.conf import settings
from django.db import models
from base.models import Timestamp 
from .orders import Order
class Refund(Timestamp):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    reason = models.TextField()
    accepted = models.BooleanField(default=False)
    price = models.FloatField()

    def __str__(self):
        return f"{self.pk}"