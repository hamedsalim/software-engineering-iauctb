from base.models import Profile
from django.db import models
from django.core.validators import RegexValidator
class CustommerProfile(Profile):
    AddressArea_services = models.ForeignKey('base.AddressArea' ,on_delete=models.CASCADE,related_name='+')
    shabaNumberRegex = RegexValidator(regex = r"\d{16}$")
    shaba_number = models.CharField(validators = [shabaNumberRegex], max_length = 16, unique = True)
    